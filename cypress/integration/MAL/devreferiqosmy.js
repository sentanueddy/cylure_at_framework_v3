﻿describe('MAL-DEV-referiqosmy E2E Regression', () => {
  context('TS01-Test on Desktop', () => {
      beforeEach(() => {
        // run these tests as if in a desktop
        // browser with a 720p monitor
        cy.viewport(1280, 720)
      })
  
  it('TC01-Validate Title', () => {
    cy.visit('https://dev.referiqosmy.com/')
    cy.title().should('eq','Age Verification - IQOS Member²')
    cy.screenshot()
  })

})

})
