﻿Cypress.on('uncaught:exception', (err, runnable) => {
  return false;
});

describe('MAL-PRD-referiqosmy E2E Regression', () => {
  context('TS01_00-LandingPage', () => {
    beforeEach(() => {
      // run these tests as if in a desktop
      // browser with a 720p monitor
      cy.viewport(1280, 720)
      cy.visit('https://referiqosmy.com/')
    })

    it('TC01_01-Validate Title', () => {
      cy.title().should('eq', 'Age Verification - IQOS Member²')
      cy.screenshot()
    })
    it('TC01_02-Validate Main Logo', () => {
      cy.get('.col-md-12 img')
        .should('have.attr', 'src')
        .and('include', 'logo')
      cy.screenshot()
    })
    it('TC01_03-Validate Language Selector', () => {
      cy.get('.dropdown select')
        .select('English')
        .select('Bahasa Melayu')
      cy.screenshot()
    })
    it('TC01_04-Input Number', () => {
      cy.get('#y1').type('9')
      cy.get('#y2').type('0')
      cy.get('#m1').type('1')
      cy.get('#m2').type('1')
      cy.get('#d1').type('1')
      cy.get('#d2').type('1')
      cy.get('#x1').type('1')
      cy.get('#x2').type('1')
      cy.get('#z1').type('1')
      cy.get('#z2').type('1')
      cy.get('#z3').type('1')
      cy.get('#z4').type('1')
      cy.get('#confirm').click()
      cy.wait(2000)
      cy.screenshot()
    })
  })

  context('TS02_00-MemberPage', () => {
    beforeEach(() => {
      // run these tests as if in a desktop
      // browser with a 720p monitor
      cy.viewport(1280, 720)
      cy.visit('https://referiqosmy.com/')
      cy.get('#y1').type('9')
      cy.get('#y2').type('0')
      cy.get('#m1').type('1')
      cy.get('#m2').type('1')
      cy.get('#d1').type('1')
      cy.get('#d2').type('1')
      cy.get('#x1').type('1')
      cy.get('#x2').type('1')
      cy.get('#z1').type('1')
      cy.get('#z2').type('1')
      cy.get('#z3').type('1')
      cy.get('#z4').type('1')
      cy.get('#confirm').click()
    })
    it('TC02_01-Validate Logo Iqos Member', () => {
      cy.wait(1500)
      cy.get('.logoiqos')
        .should('have.attr', 'src')
        .and('include', 'iqos-member')
      cy.screenshot()
    })
    it('TC02_02-Validate Member Intro', () => {
      cy.wait(1500)
      cy.get('#intro2')
        .should('contain', 'GET YOUR FIRST IQOS DEVICE')
      cy.screenshot()
    })
    it('TC02_03-Validate CTA top left', () => {
      cy.wait(1500)
      cy.get('.leftbutton')
        .should('contain', 'ENJOY 2X')
      cy.get('a[href*="how_it_works"]').click()
      cy.screenshot()
    })
    it('TC02_04-Validate CTA top right', () => {
      cy.wait(1500)
      cy.get('.rightbutton')
        .should('contain', 'FIND OUT')
      cy.get('a[href*="faq"]').click()
      cy.screenshot()
    })
  })
  context('TS03_00-RejectionPage', () => {
    beforeEach(() => {
      // run these tests as if in a desktop
      // browser with a 720p monitor
      cy.viewport(1280, 720)
      cy.visit('https://referiqosmy.com/')
      cy.get('#y1').type('1')
      cy.get('#y2').type('6')
      cy.get('#m1').type('1')
      cy.get('#m2').type('1')
      cy.get('#d1').type('1')
      cy.get('#d2').type('1')
      cy.get('#x1').type('1')
      cy.get('#x2').type('1')
      cy.get('#z1').type('1')
      cy.get('#z2').type('1')
      cy.get('#z3').type('1')
      cy.get('#z4').type('1')
      cy.get('#confirm').click()
    })
    it('TC03_01-Customer Below Age', () => {
      cy.get('#age-gate h1')
        .should('contain', 'OOPS!')
      cy.screenshot()
    })
  })

})
